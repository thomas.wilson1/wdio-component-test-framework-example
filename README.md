# wdio-component-test-framework-example

## Pre-Requisites
- Docker should be installed on your machine
- node.js v12.16.1 or greater

## Overview
This is a sample script to run WebDriverIO automated tests scripts in docker,
which includes the compose file and the package.json.

## Executing the Tests

### Docker Compose

```zsh
export TEST_SUITE=functional
docker-compose -f docker-compose.yml up --exit-code-from api-tests --force-recreate --build
```

### RunTests.sh

#### Running functional test suite
```zsh
./RunTests.sh functional
```
#### Running accessibility test suite
```zsh
./RunTests.sh accessibility
```
