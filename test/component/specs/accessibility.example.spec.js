const PageObjectFactory = require('../ui/page-object-factory.js');
const AxeUtil = require("../utils/axe-util");

describe('Searching on Google', () => {
    it('should show list of relevant search results when I perform a google search', async () => {
        await PageObjectFactory
            .getGoogleHomePage()
            .open();
        await PageObjectFactory
            .getGoogleHomePage()
            .search('BBC');

        const result = await AxeUtil.scanForViolations();
        expect(result.violations.length).toEqual(0);
    });
});


