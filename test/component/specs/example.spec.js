const PageObjectFactory = require('../ui/page-object-factory.js');

describe('Searching on Google', () => {
    it('should show list of relevant search results when I perform a google search', async () => {
        await PageObjectFactory
            .getGoogleHomePage()
            .open();
        await PageObjectFactory
            .getGoogleHomePage()
            .search('BBC');

        await expect(PageObjectFactory
            .getGoogleSearchResultsPage()
            .firstSearchResultLink)
            .toHaveText('BBC - Home');
    });
});