const fs = require("fs");

class AxeUtil {
    async scanForViolations() {
        const data = fs.readFileSync('./node_modules/axe-core/axe.min.js', 'utf8');
        await driver.executeScript(data.toString(), []);
        const result = await driver.executeAsyncScript('var callback = arguments[arguments.length - 1];axe.run().then(results => callback(results))', []);

        if (result.violations.length > 0) {
            console.log(JSON.stringify(result.violations, null, 2));
        }

        return result;
    }
}

module.exports = new AxeUtil();


