const GoogleHomePage = require('./pages/google-home.page.js');
const GoogleSearchResultsPage = require('./pages/google-search-results.page.js');

class PageObjectFactory {
    getGoogleHomePage() {
        return GoogleHomePage;
    }
    getGoogleSearchResultsPage() {
        return GoogleSearchResultsPage;
    }
}

module.exports = new PageObjectFactory();