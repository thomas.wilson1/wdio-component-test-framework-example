const Page = require('./page');

class GoogleHomePage extends Page{
    get iAgreeCookieBtn () { return $('#L2AGLb > div') }
    get searchBox () { return $('/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input') }


    async open () {
        await super.open(`https://www.google.co.uk`)
    }

    async search (textToSearch) {
        await this.iAgreeCookieBtn.click();
        await this.searchBox.setValue(textToSearch);
        await browser.keys(['Meta', 'Enter'])
    }
}

module.exports = new GoogleHomePage();