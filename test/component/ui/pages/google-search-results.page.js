const Page = require('./page');

class GoogleSearchResultsPage extends Page{
    get firstSearchResultLink () { return $('#rso > div:nth-child(1) > div > div > div > div > div > div > div.yuRUbf > a > h3') }
}

module.exports = new GoogleSearchResultsPage();